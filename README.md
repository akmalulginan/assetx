This is a template for a Docker Image Source Code (disc) repository.

Fork this repo when starting from scratch. For existing repository, you may need only these files:

- .gitlab-ci.yml              # Bridge job triggers the downstream Gitlab multi-project pipeline.
- Dockerfile                  # Any programming language specific should be abstracted by Docker.
- api.json                    # Format according to https://github.com/OAI/OpenAPI-Specification/
- metadata.json               # JSON here will be stored as is and provided as in from Asset API.

Make sure your repo is at https://gitlab.com/aai-livinglabs/disc-projects for proper access.

It is possible to have your repo outside aai-livinglabs, but you still need access to aai-livinglabs
for Gitlab pipeline run properly. Please contact us if you cannot trigger the downstream pipeline.

Currently we only support different group but with same server host, gitlab.com

It is possible in future to support different group with different server host.

## Example
- https://gitlab.com/feedloop_io/template is a fork from aai-livinglabs
- After fork, we can trigger the pipeline (https://gitlab.com/feedloop_io/template/-/pipelines/new) or let any push to main branch trigger it.
- We also get the imageurl, 959896818063.dkr.ecr.ap-southeast-1.amazonaws.com/aai-livinglabs-gitlab-com-feedloop-io-template
- We can pull it (docker pull imageurl), run it (run --network=host -p 5000:5000 imageurl), and check it via browser at http://0.0.0.0:5000
- We can also check the LivingLabs dashboard / backend API using the imageid, aai-livinglabs-gitlab-com-feedloop-io-template